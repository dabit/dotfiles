# MacVim as the main git editor
export EDITOR=code
set -o vi

# Colored grep results
export GREP_OPTIONS="--colour"

# https://gist.github.com/1688857
#export RUBY_GC_MALLOC_LIMIT=1000000000
#export RUBY_FREE_MIN=500000
#export RUBY_HEAP_MIN_SLOTS=40000

# rbenv
#export PATH="$HOME/.rbenv/bin:$PATH"

# asdf
export PATH="$HOME/.asdf/shims:$PATH"

export PATH="/usr/local/bin:$PATH"
export PATH=$PATH:/usr/local/opt/go/libexec/bin

source ~/.aliases

# Setup brew paths
eval $(/opt/homebrew/bin/brew shellenv)

export OLDPROMPT='
$fg_bold[yellow]%~ $(git_prompt_info) $reset_color($fg_bold[red]$(/usr/local/bin/rbenv version-name)$reset_color)
$ '

export PROMPT='
$fg_bold[yellow]%~ $(git_prompt_info)$reset_color
$ '

# vi on command line
bindkey -v
bindkey '^R' history-incremental-search-backward

# zsh
alias vim="stty stop '' -ixoff ; vim"
# `Frozing' tty, so after any command terminal settings will be restored
ttyctl -f

### Added by the Heroku Toolbelt
export PATH="/Applications/Postgres.app/Contents/MacOS/bin:/usr/local/heroku/bin:$PATH"

### pyenv
# export PATH="$(pyenv root)/shims:$PATH"

#nvm
# export NVM_DIR="$HOME/.nvm"
#  [ -s "/opt/homebrew/opt/nvm/nvm.sh" ] && \. "/opt/homebrew/opt/nvm/nvm.sh"  # This loads nvm
#  [ -s "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm" ] && \. "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm"  # This loads nvm bash_completion

if type brew &>/dev/null; then
  FPATH=$(brew --prefix)/share/zsh/site-functions:$FPATH
fi

# heroku autocomplete setup
HEROKU_AC_ZSH_SETUP_PATH=/Users/dabit/Library/Caches/heroku/autocomplete/zsh_setup && test -f $HEROKU_AC_ZSH_SETUP_PATH && source $HEROKU_AC_ZSH_SETUP_PATH;

# source /usr/local/etc/profile.d/z.sh
source /opt/homebrew/etc/profile.d/z.sh

#export FZF_DEFAULT_COMMAND='fd --type f'
export FZF_DEFAULT_COMMAND='ag -g ""'
# export RUBYOPT='-W:no-deprecated -W:no-experimental'
export EDITOR="code -w"


# source ~/git/sandboxd/sandboxd



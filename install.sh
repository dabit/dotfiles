#!/bin/bash

ln -s $PWD/aliases ~/.aliases
ln -s $PWD/gemrc ~/.gemrc
ln -s $PWD/getmailrc ~/.getmailrc
ln -s $PWD/gitconfig ~/.gitconfig
ln -s $PWD/gitignore_global ~/.gitignore_global
ln -s $PWD/screenrc ~/.screenrc
ln -s $PWD/tmux.conf ~/.tmux.conf
ln -s $PWD/zshenv ~/.zshenv
ln -s $PWD/profile ~/.profile
ln -s $PWD/sandboxrc ~/.sandboxrc
